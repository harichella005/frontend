<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Internal CSS</title>
</head>
<body>
    <p>This is an Example for Internal CSS</p>
    <table align="left" border="1" cellpadding="3" cellspacing="0">
        <!-- tr is for row in HTML table -->
        <tr>
            <!-- th is for table Heading and td for table cell -->
            <th>Elements</th>
            <th>Used For</th>
        </tr>
        <?php
            $a ='&lt;strong&gt;';
            $b ='It is used to bold the text';
            $c ='&lt;strong&gt;';
            $d ='It is used to bold the text';
            $e ='&lt;strong&gt;';
            $f ='It is used to bold the text';
            echo"<tr>";
            echo"<td>$a</td>";
            echo"<td>$b</td>";
            echo"</tr>";
            echo"<tr>";
            echo"<td>$c</td>";
            echo"<td>$d</td>";
            echo"</tr>";
            echo"<tr>";
            echo"<td>$e</td>";
            echo"<td>$f</td>";
            echo"</tr>";
        ?>
    </table>
</body>
</html>