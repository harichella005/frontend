<?php
if ($_SERVER['REQUEST_METHOD'] === 'GET') {
    // Load the data from the JSON file
    $jsonData = file_get_contents('data.json');
    $data = json_decode($jsonData, true);

    // Loop through the data and display each record
    foreach ($data as $record) {
        $name = $record['name'];
        $id = $record['id'];
        $age = $record['age'];
        $mail = $record['mail'];
        $phone = $record['phone'];

        // Display the record data
        echo "Name: " . $name . "<br>";
        echo "ID: " . $id . "<br>";
        echo "Age: " . $age . "<br>";
        echo "Email: " . $mail . "<br>";
        echo "Phone: " . $phone . "<br><br>";
    }
}
?>