<?php
if (isset($_POST['name']) && isset($_POST['id']) && isset($_POST['age']) && isset($_POST['mail']) && isset($_POST['phone'])) {
    $data = array(
        'name' => $_POST['name'],
        'id' => $_POST['id'],
        'age' => $_POST['age'],
        'mail' => $_POST['mail'],
        'phone' => $_POST['phone']
    );

    // Load existing data from the JSON file
    $jsonData = file_get_contents('data.json');
    $existingData = json_decode($jsonData, true);

    // Add the new data to the array
    $existingData[] = $data;

    // Convert the data back to JSON
    $jsonData = json_encode($existingData, JSON_PRETTY_PRINT);

    // Save the updated JSON data to the file
    file_put_contents('data.json', $jsonData);

    echo "Data saved successfully!";
}
?>