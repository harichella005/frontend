<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../css/style.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-9ndCyUaIbzAi2FUVXJi0CjmCapSmO7SnpJef0486qhLnuZ2cdeRhO02iuK6FUUVM" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-geWF76RCwLtnZ8qwWowPQNguL3RmwHVBC9FhGdlKrxdiJJigb/j/68SIy3Te4Bkz"
        crossorigin="anonymous"></script>
    <title>HTML CSS and JavaScript</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script>
        let list = $("#list");
        let listItems = $("#list .item");
        $(window).resize(function () {
            let listPosition = list.position().top;
            let hiddenItems = listItems.filter(function () {
                return $(this).position().top - listPosition > 0;
            }).clone();
        });
    </script>
</head>

<body>
    <nav class="navbar fixed-top">
        <div class="container-fluid">
            <a class="navbar-brand" href="#">Intechwings</a>
            <button id="navbar-toggler" class="navbar-toggler" type="button" data-bs-toggle="offcanvas"
                data-bs-target="#offcanvasNavbar" aria-controls="offcanvasNavbar" aria-label="Toggle navigation">
                <span id="navbar-toggler-icon" class="navbar-toggler-icon"></span>
            </button>
            <div class="offcanvas offcanvas-end" tabindex="-1" id="offcanvasNavbar"
                aria-labelledby="offcanvasNavbarLabel">
                <div class="offcanvas-header">
                    <h2 class="offcanvas-title" id="offcanvasNavbarLabel">Basics of HTML</h2>
                    <button type="button" class="btn-close" data-bs-dismiss="offcanvas" aria-label="Close"></button>
                </div>
                <div class="offcanvas-body">
                    <ul class="navbar-nav justify-content-end flex-grow-1 pe-3">
                        <li class="nav-item">
                            <a class="nav-link" href="#">HTML Introduction</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Editor</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">HTML Attributes</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Favicon and Title in HTML</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Heading in HTML</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Paragraph in HTML</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">HTML Formating</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Colors in HTML</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Style CSS</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Links in HTML - Anchor</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Table in HTML</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Editor</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </nav>
    <div class="body-content">
        <!-- This div is for the introduction of HTML -->
        <div class="introduction">
            <h1>HTML Introduction</h1>
            <p>HTML is a markup language used to create a basic webpages</p>
            <ol>
                <li>HTML stands for Hyper Text Markup Language</li>
                <li>It describes the structure of a webpage (skeletor)</li>
                <li>HTML consist of a series of elements and the elements will tell the browser how to display the
                    content</li>
                <li>HTML makes it possible to create static pages with text, headings, tables, lists, images, links, and
                    so on</li>
            </ol>
            <h2>What is Web Browser?</h2>
            <p>A web browser like (Firefox, chrome, edge, safari) takes you anywhere on the internet. It retrieves
                information from other parts of the web and displays it on your desktop or mobile device. The
                information is transferred using the Hypertext Transfer Protocol, which defines how text, images and
                video are transmitted on the web. It will not display the HTML tags, but uses them to determine how to
                dislay the content from the document</p>
            <!-- This div is for the HTML code snippet and example -->
            <div>
                <h1>HTML Code Snippet and Sample Program</h1>
                <p>&lt;!DOCTYPE html&gt;<br />&lt;html&gt;<br />&lt;head&gt;<br />&nbsp;&nbsp;&nbsp;&nbsp;
                    &lt;title&gt;Title for the HTML
                    page&lt;/title&gt;<br />&lt;/head&gt;<br />&lt;body&gt;<br />&nbsp;&nbsp;&nbsp;&nbsp;
                    &lt;h1&gt;Heading&lt;/h1&gt;<br />&nbsp;&nbsp;&nbsp;&nbsp;
                    &lt;p&gt;Paragraph&lt;/p&gt;<br />&nbsp;&nbsp;&nbsp;&nbsp; &lt;a&gt;URL
                    link&lt;/a&gt;<br />&lt;/body&gt;<br />&lt;/html&gt;</p>
                <br />
                <h5>Note: Don't try to work on code this is introduction <br>try to understand practical will start from
                    next to the element syntax</h5>
                <h2>Output for the above code</h2>
                <img src="./assests/Html code sn ippet and sample code.png" alt="">
                <h2>Explanation for Example</h2>
                <ol>
                    <li>The &lt;!DOCTYPE html&gt; defines that the current document is an HTML5</li>
                    <li>The &lt;html&gt; is the root element of the HTML page</li>
                    <li>The &lt;head&gt; will contain the meta information about the HTML page</li>
                    <li>The &lt;title&gt; element specifies the title of the current HTML page</li>
                    <li>The &lt;body&gt; element will defines the documents body, and is a container for all the visible
                        contents in browser such as headings, paragraphs, images, hypherlinks, lists, tables, videos,
                        etc..,
                    </li>
                    <li>The &lt;h1&gt; defines the large heading there are more headings formats like h1, h2, h3, h4,
                        h5,
                        and h6</li>
                    <li>The &lt;p&gt; element will display the paragraph content </li>
                    <li>The &lt;a&gt; defines the hyperlink</li>
                </ol>
            </div>
            <!-- This div for comments -->
            <div>
                <h1>Comments in HTML</h1>
                <p>As a Developer or an Software Engineer who write code they must have an indepth knowledge of comments
                </p>
                <p>Basically when writing a big application or small application or some complex application, multiple
                    person can work on a single project and everyday have type atleast 100 lines of code, here were the
                    comments works, each line of some function created by develeoper he/she have to give the description
                    of
                    single line then only any one can understand the code easily by seeing it otherwise its complecated
                    to
                    understand.</p>
                <p>HTML comments are not displayed in the browser, but they can help document your HTML source code.</p>
                <p>Appears in the green line</p>
                <p>The syntax used for comments in HTML is <br><br>&nbsp;&nbsp;&lt;!-- Your comment --&gt; </p>
                <img src="./assests/comments.png" alt="">
            </div>
            <!-- This is for the elements syntax and explanation -->
            <div>
                <h1>HTML Element and Syntax</h1>
                <h3>What is HTML Element?</h3>
                <p>An HTML element is a tagname that defines by the starting tag, content, and an ending tag.</p>
                <h4>Syntax: </h4>
                <p>&lt;tagname&gt;Content.......&lt;tagname&gt;</p>
                <table style="border-collapse: collapse; width: 100%; height: 100px;" border="1">
                    <tbody>
                        <tr style="height: 21px;">
                            <td style="width: 33.3333%; height: 21px; text-align: center;">
                                <h3><strong>Start tag</strong></h3>
                            </td>
                            <td style="width: 33.3333%; height: 21px; text-align: center;">
                                <h3><strong>Element content</strong></h3>
                            </td>
                            <td style="width: 33.3333%; height: 21px; text-align: center;">
                                <h3><strong>End tag</strong></h3>
                            </td>
                        </tr>
                        <tr style="height: 21px;">
                            <td style="width: 33.3333%; height: 21px;">&lt;p&gt;</td>
                            <td style="width: 33.3333%; height: 21px;">Content of the paragraph</td>
                            <td style="width: 33.3333%; height: 21px;">&lt;/p&gt;</td>
                        </tr>
                        <tr style="height: 21px;">
                            <td style="width: 33.3333%; height: 21px;">&lt;h2&gt;</td>
                            <td style="width: 33.3333%; height: 21px;">h2 content</td>
                            <td style="width: 33.3333%; height: 21px;">&lt;/h2&gt;</td>
                        </tr>
                        <tr style="height: 21px;">
                            <td style="width: 33.3333%; height: 21px;">&lt;input /&gt;</td>
                            <td style="width: 33.3333%; height: 21px;">none</td>
                            <td style="width: 33.3333%; height: 21px;">none</td>
                        </tr>
                    </tbody>
                </table>
                <h3>What will happen when we skip the end tag in mandatory field?</h3>
                <p>The browser will understand that the tag is like above one and render the result. Have a look below
                    code
                    <br> I have removed the close tag for h1 and the browser will understand that the below &lt;p&gt;
                    tag is
                    like above &lt;h1&gt; <br> and its continuation
                </p>
                <p>&lt;!DOCTYPE html&gt;<br />&lt;html&gt;<br />&lt;head&gt;<br />&nbsp;&nbsp;&nbsp;&nbsp;
                    &lt;title&gt;Title for the HTML
                    page&lt;/title&gt;<br />&lt;/head&gt;<br />&lt;body&gt;<br />&nbsp;&nbsp;&nbsp;&nbsp;
                    &lt;h1&gt;Heading
                    (i have removed the close tag of h1)<br />&nbsp;&nbsp;&nbsp;&nbsp;
                    &lt;p&gt;Paragraph&lt;/p&gt;<br />&nbsp;&nbsp;&nbsp;&nbsp; &lt;a&gt;URL
                    link&lt;/a&gt;<br />&lt;/body&gt;<br />&lt;/html&gt;</p>
                <br>
                <h2>Output of the above code</h2>
                <img src="./assests/noendtag.png" alt="">
                <h5>Note: Browser understand that the below &lt;p&gt; tag is continuation of &lt;h1&gt;</h5>
            </div>
            <!-- For next and previous button -->
            <div class="prev-next">
                <a href="/" class="prev btns">Previous</a>
                <a href="../html/editor.html" class="next btns">Next</a>
            </div>
        </div>
        <!-- This div is for the Editor -->
        <hr id="thread4">
        <div>
            <h1>Editor</h1>
            <h3>What is Editor and IDE?</h3>
            <p>A text editor refers to any form of computer program that enables users to create, change, edit, open and
                view plain text files. They come already installed on most operating systems but their dominant
                application has evolved from notetaking and creating documents to crafting complex code.</p>
            <p>An integrated development environment (IDE) is a software application that helps programmers develop
                software code efficiently. It increases developer productivity by combining capabilities such as
                software editing, building, testing, and packaging in an easy-to-use application. Just as writers use
                text editors and accountants use spreadsheets, software developers use IDEs to make their job easier.
            </p>
            <h5>Note: You can use any of the text editor to code the html but my recommendation is Visual Studio Code
                IDE</h5>
            <br>
            <h3>Steps to install the VS code IDE</h3>
            <ul>
                <li>
                    <p>Visit <a href="https://code.visualstudio.com/download">https://code.visualstudio.com/download</a>
                    </p>
                </li>
                <li>Download the package depends upon your OS ( Im using Ubuntu 22.04.2 )</li>
                <li>
                    <p>sudo apt install ./&lt;file&gt;.deb</p>
                </li>
                <li>
                    <p>If you're on an older Linux distribution, you will need to run this instead:</p>
                </li>
                <li>
                    <p>sudo dpkg -i &lt;file&gt;.deb</p>
                </li>
                <li>
                    <p>sudo apt-get install -f # Install dependencies</p>
                </li>
            </ul>
            <h3>After Installing the VS Code IDE</h3>
            <ul>
                <li>
                    <p>Create a new project repository and create a new file and name it as "index.html" or index.htm (
                        maintain the consistency )</p>
                </li>
                <li>
                    <p>Write your own code in the index.html file, you can also try the above example</p>
                </li>
                <li>
                    <p>Save the file and open with your browser, it will render the content and shows the result WYSIWYG
                    </p>
                </li>
            </ul>
        </div>
        <!-- This div for HTML Attributes -->
        <hr id="thread4">
        <div>
            <h1>HTML Attributes</h1>
            <p>The HTML attributes will provide the additional information about the elements</p>
            <ul>
                <li>
                    <p>HTML attributes are special words used inside the opening tag to control the element's behaviour.
                    </p>
                </li>
                <li>
                    <p>HTML attributes are a modifier of a HTML element type and all HTML elements can have
                        <strong>attributes</strong>
                    </p>
                </li>
                <li>
                    <p>The attributes will looks like <strong>name="value"</strong></p>
                </li>
            </ul>
            <br>
            <h2>Each HTML tags will use different attributes Will see later</h2>
        </div>
        <hr id="thread4">
        <!-- This div is for Favicon and Page Title-->
        <div>
            <h1>Favicon and Title in HTML</h1>
            <p>A favicon is a small image displayed next to the page title in the browser tab. This also known as a tab
                icon, URL icon, website icon, or a bookmark icon.</p>
            <p>The favicon image format is <code>.ico</code> and there are various file format like png, jpg, gif, and
                svg also used, but the <code>.ico</code> is supported by all the browsers.</p>
            <h2>Where to add this favicon in HTML document and How?</h2>
            <p>In the HTML document the favicon is added in the &lt;head&gt; section using the &lt;link&gt; element. It
                follows the below syntax:</p>
            <p>&lt;head&gt;</p>
            <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &lt;link rel="icon" type="image/icon" href="url/image path"&gt;</p>
            <p>&lt;/head&gt;</p>
            <p>Here the <code>link</code> element will used to link any file</p>
            <p>The <code>rel</code> attribute specifies the relationship between the current and the linked document and
                this is only used if the <code>href</code> attribute is present</p>
            <p>In our case the <code>rel="icon"</code> so its going to contain the icon file</p>
            <p>The <code>type</code> attribute is used to mention the type of the files used in <code>link</code></p>
            <h2>Below image shows the result of Favicon </h2>
            <img src="./assests/faviconout.png" alt="">
            <h2>Example code for Favicon</h2>
            <img src="./assests/faviconcode.png" alt="">

        </div>
        <!-- This div is for all the headings from h1 to h6 -->
        <hr id="thread4">
        <div>
            <h1>Heading in HTML</h1>
            <p>The HTML headings are titles or subtitles that we want to display on a webpage (Same as in text editor)
            </p>
            <p>There are totally 6 headings from &lt;h1&gt; to &lt;h6&gt; tags</p>
            <p>&lt;h1&gt; most important heading and &lt;h6&gt; is the least important heading</p>
            <h1>Heading 1</h1>
            <h2>Heading 2</h2>
            <h3>Heading 3</h3>
            <h4>Heading 4</h4>
            <h5>Heading 5</h5>
            <h6>Heading 6</h6>
            <br>
            <img src="./assests/headingsh1toh6.png" alt="">
        </div>
        <hr id="thread4">
        <!-- This div is for paragraph -->
        <div>
            <h1>Paragraph in HTML</h1>
            <p>A paragraph always starts on a new line, and browsers automatically add some white space before and after
                a paragraph. </p>
            <p>Adding extra spaces or extra lines in the &lt;p&gt; tag that will not displayed, browser will
                automatically detect and remove the spaces and lines</p>
            <p>If you need additional spaces and lines there is an alternative ways, you can add html character entity
                <strong>&amp;nbsp;</strong>its for space we will see indepth of html entities later
            </p>
            <p>The &lt;br&gt; element is used to break the line</p>
            <br>
            <p>The following code shows the result for adding extra spaces and lines:</p>
            <p>&lt;!DOCTYPE html&gt;<br />&lt;html&gt;<br />&lt;head&gt;<br />&nbsp;&nbsp;&nbsp;&nbsp;
                &lt;title&gt;Paragraph&lt;/title&gt;<br />&lt;/head&gt;<br />&lt;body&gt;<br />&nbsp;&nbsp;&nbsp;&nbsp;
                &lt;p&gt;This tag is used to display the paragraph&lt;/p&gt;<br />&nbsp;&nbsp;&nbsp;&nbsp; &lt;!-- Added
                extra lines and spaces in the <br />&nbsp;&nbsp;&nbsp;&nbsp; paragraph, but the web browser will
                automatically<br />&nbsp;&nbsp;&nbsp;&nbsp; detect and remove it --&gt;<br />&nbsp; &nbsp;&nbsp;
                &lt;p&gt;Lorem ipsum, dolor sit amet consectetur adipisicing elit.
                <br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Suscipit
                repudiandae aliquid autem blanditiis fugit voluptate
                <br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; quaerat architecto
                ipsa sequi ea sapiente, fugiat eaque fuga
                <br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; repellat at
                laudantium tenetur accusamus quo!&lt;/p&gt;<br />&nbsp;&nbsp;&nbsp;&nbsp; &lt;p&gt;Here is extra
                &nbsp;&nbsp;&nbsp;&nbsp; space&lt;/p&gt;<br />&lt;/body&gt;<br />&lt;/html&gt;
            </p>
            <h2>Output of the above code</h2>
            <img src="./assests/paragraphextraspacesandlines.png" alt="">
            <h3>Solution for this example you are writing a poem or any quotes how to maintain the correct spaces and
                lines</h3>
            <p>Using the &lt;pre&gt;content...&lt;/pre&gt; we can write the quotes or a poem</p>
            <p>The HTML &lt;pre&gt; element defines preformatted text. The text inside a &lt;pre&gt; element is
                displayed in a fixed-width font (usually Courier), and it preserves both spaces and line breaks:</p>
            <img src="./assests/paragraphpre.png" alt="">
        </div>
        <hr id="thread4">
        <!-- This div is for the HTML Formatting tags -->
        <div>
            <h1>HTML Formating</h1>
            <p>HTML Formatting is a process of formatting text for better look and feel. HTML provides us ability to
                format text without using CSS. There are many formatting tags in HTML. These tags are used to make text
                bold, italicized, or underlined. There are almost 14 options available that how text appears in HTML and
                XHTML.</p>
            <p>The formating elements are designed to display the special types of text. Listed below are the elements
                mostly used: </p>
            <br>
            <table style="border-collapse: collapse; width: 100%; height: 210px;" border="1">
                <tbody>
                    <tr style="height: 21px;">
                        <td style="width: 50%; height: 21px;">
                            <h3 style="text-align: center;"><strong>Elements</strong></h3>
                        </td>
                        <td style="width: 50%; height: 21px;">
                            <h3 style="text-align: center;"><strong>Used For</strong></h3>
                        </td>
                    </tr>
                    <tr style="height: 21px;">
                        <td style="width: 50%; height: 21px;">&lt;b&gt;</td>
                        <td style="width: 50%; height: 21px;">Bold text</td>
                    </tr>
                    <tr style="height: 21px;">
                        <td style="width: 50%; height: 21px;">&lt;strong&gt;</td>
                        <td style="width: 50%; height: 21px;">Important text</td>
                    </tr>
                    <tr style="height: 21px;">
                        <td style="width: 50%; height: 21px;">&lt;i&gt;</td>
                        <td style="width: 50%; height: 21px;">Italic text</td>
                    </tr>
                    <tr style="height: 21px;">
                        <td style="width: 50%; height: 21px;">&lt;em&gt;</td>
                        <td style="width: 50%; height: 21px;">Emphasized text</td>
                    </tr>
                    <tr style="height: 21px;">
                        <td style="width: 50%; height: 21px;">&lt;mark&gt;</td>
                        <td style="width: 50%; height: 21px;">Marked text</td>
                    </tr>
                    <tr style="height: 21px;">
                        <td style="width: 50%; height: 21px;">&lt;u&gt;</td>
                        <td style="width: 50%; height: 21px;">Underline text</td>
                    </tr>
                    <tr style="height: 21px;">
                        <td style="width: 50%; height: 21px;">&lt;big&gt;</td>
                        <td style="width: 50%; height: 21px;">Bigger text</td>
                    </tr>
                    <tr style="height: 21px;">
                        <td style="width: 50%; height: 21px;">&lt;small&gt;</td>
                        <td style="width: 50%; height: 21px;">Smaller text</td>
                    </tr>
                    <tr style="height: 21px;">
                        <td style="width: 50%; height: 21px;">&lt;del&gt;</td>
                        <td style="width: 50%; height: 21px;">Deleted text</td>
                    </tr>
                    <tr style="height: 21px;">
                        <td style="width: 50%; height: 21px;">&lt;ins&gt;</td>
                        <td style="width: 50%; height: 21px;">Inserted text</td>
                    </tr>
                    <tr style="height: 21px;">
                        <td style="width: 50%; height: 21px;">&lt;sub&gt;</td>
                        <td style="width: 50%; height: 21px;">Subscript text</td>
                    </tr>
                    <tr style="height: 21px;">
                        <td style="width: 50%; height: 21px;">&lt;sup&gt;</td>
                        <td style="width: 50%; height: 21px;">Superscript text</td>
                    </tr>
                </tbody>
            </table>
            <br>
            <h2>Bold and Strong Text</h2>
            <p>The &lt;b&gt; element display the text or sentence in bold without any importance, but the &lt;strong&gt;
                will gives the importance for the text.</p>
            <br>
            <img src="./assests/bandstrong.png" alt="">
            <h2>Italic and Emphasized Text</h2>
            <p>The &lt;i&gt; element display the text or a sentence in the italic and the &lt;em&gt; element defines the
                emphasized text.</p>
            <br>
            <img src="./assests/italicem.png" alt="">
            <br>
            <h2>Mark Text or Sesntnce</h2>
            <p>The &lt;mark&gt; element defines the text that should be marked or highlighted. For example while reading
                a book an important line or like sentence will come we highlight it same as that.</p>
            <br>
            <img src="./assests/marktext.png" alt="">
            <br>
            <h2>Underline Text</h2>
            <p>The element &lt;u&gt; used to underline the text</p>
            <img src="./assests/underlinetext.png" alt="">
            <br>
            <h2>Big and Small Text</h2>
            <p>The &lt;big&gt; element is used for displaying the bigger text and the &lt;small&gt; element is used to
                display the smaller text.</p>
            <p>Note: The &lt;big&gt; element is from HTML 4</p>
            <img src="./assests/bigandsmalltext.png" alt="">
            <br>
            <h2>Deleted Text</h2>
            <p>The &lt;del&gt; element is used to define the deleted text from the document. For example, if you are
                writing something, instead of writing "Good Morning" written "Hood Morning", Here we just strike the
                word and rewrite that.</p>
            <p>You can use the &lt;ins&gt; to insert the new word</p>
            <p>Note: &lt;ins&gt; render the output same as underline</p>
            <br>
            <img src="./assests/Deletedtext.png" alt="">
            <br>
            <h2>Subscript and Superscript Text</h2>
            <p>The &lt;sub&gt; element defines subcript text. It appears half a character below the normal line, and is
                sometimes rendered in a smaller font Example H<sub>2</sub>O</p>
            <p>The &lt;sup&gt; element defines supercript text. It appears half a character above the normal line, and
                is sometimes rendered in a smaller font</p>
            <img src="./assests/subsuptext.png" alt="">
        </div>
        <hr id="thread4">
        <!-- This div for HTML colors -->
        <div>
            <h1>Colors In HTML</h1>
            <p>The HTML Colors are specified with the predefined color name otherwise we can use the colors with (RBG,
                HEX, HSL, HSLA, or RGBA values.)</p>
            <p>We can set color for text, background of the text, button, div and soon..</p>
            <h2>Background Color</h2>
            <p>The background of the div, button, text and more can be achieved by using the style element.</p>
            <img src="./assests/bgcolorcode.png" alt="">
            <h3>Output of the above code</h3>
            <br>
            <img src="./assests/bgcolorout.png" alt="">
        </div>
        <hr id="thread4">

        <!-- This div is for Style CSS in HTML  -->
        <div>
            <h1>Styles CSS</h1>
            <p>Css is the expansion of Cascading Style Sheet (CSS), this mainly focus on the color, format the layout of
                a webpages. This will saves a lot of working time and give the good UI for the webpage </p>
            <p>Without css you can't develope an webpage, if we done without css it will be just like the book written
                in black pen without the proper formatting</p>
            <h2>Types of CSS</h2>
            <p>The CSS can be used in three different ways, the following are the 3 types:
            <ul>
                <li>
                    Inline - Using the <code>style</code> attribute inside the HTML elements
                </li>
                <li>
                    Internal - Using the &lt;style&gt; element in the &lt;head&gt; section
                </li>
                <li>
                    External - Using the &lt;link&gt; element we can add the external CSS file.
                </li>
            </ul>
            </p>
            <h2>Inline CSS</h2>
            <p>The Inline CSS is used by the style attribute in the HTML elements, we have seen this example in previous
                background colors. </p>
            <p>I will show you the sample for better understanding</p>
            <p>&lt;p style="color:#3f0363"&gt;This is an Example for inline CSS&lt;/p&gt;</p>
            <img src="./assests/inlinecss.png" alt="">
            <h2>Internal CSS</h2>
            <p>This can be achieved by using the &lt;style&gt; element in the &lt;head&gt; section.</p>
            <img src="./assests/internalcss.gif" alt="gif">
            <img src="./assests/Internalcss.png" alt="">
            <h2>External CSS</h2>
            <p>The external CSS uses the separate CSS file that includes CSS Styles for HTML pages.</p>
            <p>We can use the &lt;link&gt; element in the &lt;head&gt; section of each HTML page</p>
            <p>Give below is the result:</p>
            <img src="./assests/externalcssout.png" alt="">
            <p>The HTML file contains the simple &lt;p&gt; element, styles for this &lt;p&gt; is linked with the
                external document in the &lt;head&gt; section using the &lt;link&gt; element</p>
            <img src="./assests/externalcsshtml.png" alt="">
            <p>Below is the external CSS file named as style.css</p>
            <img src="./assests/externalcssstyle.png" alt="">
        </div>
        <hr id="thread4">
        <!-- This div is for Links in HTML -->
        <div>
            <h1>Links in HTML - Anchor</h1>
            <p>Links is called as hypherlink that is used to link another page, allow user to click their way from page
                to page using the hyperlink. </p>
            <h2>Link Syntax</h2>
            <p>&lt;a href="url"&gt;Text&lt;/a&gt;</p>
            <p>Lets have a look on &lt;a&gt; this element is used for indicating the hyperlink and the <code>href</code>
                attribute is used to hold the destination link, If the user clicks the link, button, or image, that will
                send the user to the specified URL.</p>
            <img src="./assests/linka.gif" alt="">
            <p>There are 3 different types of links are there that will appears as follows in all browsers:</p>
            <ul>
                <li>Unvisited link is underlined and blue</li>
                <li>Visited link is underlined and purple</li>
                <li>Active link is underlined and red</li>
            </ul>
            <p>We can also customise by adding some css for the color and for removing the underline </p>
            <img src="./assests/linktype3.gif" alt="">
            <h2>Example code for the above output</h2>
            <img src="./assests/linktype3.png" alt="">
            <h2>Links - Target Attribute </h2>
            <p>The linked page will be displayed in the current browser window. To change this, you must specify another
                target for the link.</p>
            <p>The target attribute specifies where to open the linked document and that can have one of the followingg
                values:</p>
            <ul>
                <li>_self - Opens the document in the same window/tab as it was clicked and this is an Default one.</li>
                <li>_blank - Opens the document in a new window/tab</li>
                <li>_parent - Opens the document in the parent frame</li>
                <li>_top - Opens the document in the full body of the window</li>
            </ul>
            <img src="./assests/linkattri4.gif" alt="">
            <h2>Example code for the above output</h2>
            <img src="./assests/linkatt4.png" alt="">
            <h2>Types of URL in HTML</h2>
            <p>In HTML there are two main types of URLs that are used depending on the starting point of access to a web
                host or a domain name, they are called <strong>absolute and relative URLs.</strong> </p>
            <ul>
                <li>
                    <p>Absolute URL - an absolute URL is the full URL, that includes the HTTP/HTTPS protocol, the
                        subdomain(mail), domain(google.com), and path (which include the directory and slug)</p>
                </li>
                <li>
                    <p>Relative URL - an relative URL is the URL that only includes the path. The path is everything
                        that comes after the domain, including the directory and slug</p>
                </li>
            </ul>
            <img src="./assests/typesofurl.gif" alt="">
            <h2>Example code for the above output</h2>
            <img src="./assests/typesurl.png" alt="">
            <h2>HTML Links - Use Button, Image, Div, Section and more as a link.</h2>
            <p>By using the &lt;a&gt; element we can use image, button, div, section and more as a link, lets have a
                quick look on it. Try yourself using the div and section</p>
            <img src="./assests/aaslink.gif" alt="">
            <h2>Example code for the above output</h2>
            <img src="./assests/aaslink.png" alt="">
            <!-- From this i used quilbot -->
            <h2>HTML Link - Bookmarks Link</h2>
            <p>To enable users to quickly navigate to particular sections of a web page, HTML links can be used to
                construct bookmarks.</p>
            <p>If we are developing very long webpage, the bookmark will be used to scroll down or up to the location
                when the link is clicked</p>
            <p>For handling this bookmark the <code>id</code> attribute will be used to create a bookmark.</p>
            <img src="./assests/linkbookmark.gif" alt="">
            <h2>Example code for the above output</h2>
            <img src="./assests/linkbookmark.png" alt="">
        </div>
        <hr id="thread4">
        <!-- This div is for Tables -->
        <div>
            <h1>Table in HTML</h1>
            <p>HTML table element is used to display the data in tabular form <code>row * column</code> and there can be
                many columns in a row.</p>
            <p>The tables are used to manage the layout of the page like navigation bar, header section, body content,
                footer section etc.</p>
            <p>To create a table in HTML a few tags are used lets see</p>
            <table style="border-collapse: collapse; width: 100.454%; height: 100px;" border="1">
                <tbody>
                    <tr style="height: 10px;">
                        <td style="width: 50%; height: 10px;">
                            <h3 style="text-align: center;"><strong>Tags</strong></h3>
                        </td>
                        <td style="width: 50%; height: 10px;">
                            <h3 style="text-align: center;"><strong>Description</strong></h3>
                        </td>
                    </tr>
                    <tr style="height: 10px;">
                        <td style="width: 50%; height: 10px;">&lt;table&gt;</td>
                        <td style="width: 50%; height: 10px;">It defines a table.</td>
                    </tr>
                    <tr style="height: 10px;">
                        <td style="width: 50%; height: 10px;">&lt;tr&gt;</td>
                        <td style="width: 50%; height: 10px;">It defines a row in a table.</td>
                    </tr>
                    <tr style="height: 10px;">
                        <td style="width: 50%; height: 10px;">&lt;th&gt;</td>
                        <td style="width: 50%; height: 10px;">It defines a header cell in a table.</td>
                    </tr>
                    <tr style="height: 10px;">
                        <td style="width: 50%; height: 10px;">&lt;td&gt;</td>
                        <td style="width: 50%; height: 10px;">It defines a cell in a table.</td>
                    </tr>
                    <tr style="height: 10px;">
                        <td style="width: 50%; height: 10px;">&lt;caption&gt;</td>
                        <td style="width: 50%; height: 10px;">It defines the table caption</td>
                    </tr>
                    <tr style="height: 10px;">
                        <td style="width: 50%; height: 10px;">&lt;colgroup&gt;</td>
                        <td style="width: 50%; height: 10px;">It specifies a group of one or more columns in a table for
                            formatting</td>
                    </tr>
                    <tr style="height: 10px;">
                        <td style="width: 50%; height: 10px;">&lt;col&gt;</td>
                        <td style="width: 50%; height: 10px;">It is used with &lt;colgroup&gt; element to specify column
                            properties for each column</td>
                    </tr>
                    <tr style="height: 10px;">
                        <td style="width: 50%; height: 10px;">&lt;tbody&gt;</td>
                        <td style="width: 50%; height: 10px;">It is used to group the body content in a table.</td>
                    </tr>
                    <tr style="height: 10px;">
                        <td style="width: 50%; height: 10px;">&lt;thead&gt;</td>
                        <td style="width: 50%; height: 10px;">It is used to group the header content in a table.</td>
                    </tr>
                    <tr style="height: 10px;">
                        <td style="width: 50%; height: 10px;">&lt;tfooter&gt;</td>
                        <td style="width: 50%; height: 10px;">It is used to group the footer content in a table.</td>
                    </tr>
                </tbody>
            </table>
            <p>Lets see the simple example</p>
            <img src="./assests/simtableout.png" alt="">
            <p>Here i have used the <code>table tr th td tbody</code> elements to create a simple table for better
                understanding</p>
            <img src="./assests/simtablecode.png" alt="">
            <ul>
                <li>
                    <p>&lt;table&gt; - This element is used to defines an HTML table </p>
                </li>
                <li>
                    <p>&lt;tbody&gt; - This element is used for grouping the body content in an HTML table.</p>
                </li>
                <li>
                    <p>&lt;tr&gt; - This element is used to define the row in an HTML table and the &lt;tr&gt; element
                        contains one or more &lt;th&gt; or &lt;td&gt; elements. </p>
                </li>
                <li>
                    <p>&lt;th&gt; - This element is used to define the header cell in an HTML Table and the HTML table
                        hase two kinds of cells</p>
                    <ul>
                        <li>
                            <p><strong>Header cell</strong> - contains header information (created with the &lt;th&gt;
                                element)</p>
                        </li>
                    </ul>
                    <ul>
                        <li>
                            <p><strong>Data cell</strong> - contains data (created with the &lt;td&gt; element)</p>
                        </li>
                    </ul>
                </li>
                <li>
                    <p>&lt;td&gt; - This defines the data cell in an HTML table. </p>
                </li>
            </ul>
            <p>Lets see another example with <code> &lt;caption&gt; &lt;colgroup&gt; &lt;col&gt; &lt;tfooter&gt;</code>
                elements</p>
            <img src="./assests/tablecolfootout.png" alt="">
            <img src="./assests/tablecolfootcode.png" alt="">
            <ul>
                <li>
                    <p>The &lt;caption&gt; tag defines a table caption and this must be inserted after the &lt;table&gt;
                        tag. By default, a caption for the table will be center-aligned above a table and the CSS
                        properties <code>text-align</code> and <code>caption-side</code> can be used to align and place
                        the caption.</p>
                </li>
                <li>
                    <p>The &lt;colgroup&gt; tag specifies a group of one or more columns in a table for formatting and
                        it is useful for applying styles to entire columns, instead of repeating the styles for each
                        cell and each row.</p>
                </li>
                <ul>
                    <li>
                        <p></p>
                    </li>
                </ul>
            </ul>

        </div>
        <footer class="footer">
            <div>
                <h1>footer</h1>
            </div>
        </footer>
    </div>
</body>

</html>