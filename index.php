<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="./css/style.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-9ndCyUaIbzAi2FUVXJi0CjmCapSmO7SnpJef0486qhLnuZ2cdeRhO02iuK6FUUVM" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-geWF76RCwLtnZ8qwWowPQNguL3RmwHVBC9FhGdlKrxdiJJigb/j/68SIy3Te4Bkz"
        crossorigin="anonymous"></script>
    <title>Home Page</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script>
        let list = $("#list");
        let listItems = $("#list .item");
        $(window).resize(function () {
            let listPosition = list.position().top;
            let hiddenItems = listItems.filter(function () {
                return $(this).position().top - listPosition > 0;
            }).clone();
        });
    </script>
</head>

<body>
    <nav class="navbar fixed-top">
        <div class="container-fluid">
            <a class="navbar-brand" href="#">Intechwings</a>
            <button id="navbar-toggler" class="navbar-toggler" type="button" data-bs-toggle="offcanvas"
                data-bs-target="#offcanvasNavbar" aria-controls="offcanvasNavbar" aria-label="Toggle navigation">
                <span id="navbar-toggler-icon" class="navbar-toggler-icon"></span>
            </button>
            <div class="offcanvas offcanvas-end" tabindex="-1" id="offcanvasNavbar"
                aria-labelledby="offcanvasNavbarLabel">
                <div class="offcanvas-header">
                    <h2 class="offcanvas-title" id="offcanvasNavbarLabel">Programming Languages</h2>
                    <button type="button" class="btn-close" data-bs-dismiss="offcanvas" aria-label="Close"></button>
                </div>
                <div class="offcanvas-body">
                    <ul class="navbar-nav justify-content-end flex-grow-1 pe-3">
                        <li class="nav-item">
                            <a class="nav-link" href="#">Computer Basics</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="../html-basics">HTML</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">CSS</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">JavaScript</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Jquery</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Bootstrap</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">PHP</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">SQL</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Python</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Linux Basics</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">ReactJS</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">NodeJS</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">C</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">C++</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </nav>
    <div class="container text-center">
        <div class="row row-cols-1">
            <a href="https://google.com">
                <div class="col">
                    <div class="container-title">
                        <h4>HTML</h4>
                    </div>
                    <div class="container-image">
                        <img src="./assests/naturepic.jpg" alt="">
                    </div>
                    <div class="container-description">
                        <p>HTML is a web creation </p>
                    </div>
                </div>
            </a>

            <div class="col">Column</div>
            <div class="col">Column</div>
            <div class="col">Column</div>
            <div class="col">Column</div>
            <div class="col">Column</div>
            <div class="col">Column</div>
            <div class="col">Column</div>
            <div class="col">Column</div>
            <div class="col">Column</div>
            <div class="col">Column</div>
            <div class="col">Column</div>
            <div class="col">Column</div>
            <div class="col">Column</div>
            <div class="col">Column</div>
            <div class="col">Column</div>
        </div>
    </div>
    <footer class="footer">
        <div>
            <h1>footer</h1>
        </div>
    </footer>
</body>

</html>